class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, new_product):
        self.products.append(new_product)


    def get_total_price(self):
        sum_of_order = 0
        if len(self.products) == 0:
            return 0
        else:
            for y in self.products:
                sum_of_order = sum_of_order + y.get_price()
            return sum_of_order


    def get_total_quantity_of_products(self):
        sum_of_order_qty = 0
        for product in self.products:
            sum_of_order_qty += product.get_product_quantity()
        return sum_of_order_qty


    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_product_quantity(self):
        return self.quantity

    def get_price(self):
       return self.unit_price * self.quantity
